# Jupyter TOC Gen

This python script can generate a markdown table of contents from the markdown of a jupyter notebook

## Installation

Clone the repository or wget the program.py file. Either run the file, or include it a jupyter notebook.

## How to Use

Having cloned the file type:

`$ python program.py -h # for help`

To generate a toc type

`$ python program.py relative/path/to/python.ipynb g #This generates the output in the console`

Choose to save the output to a file

`$ python program.py relative/path/to/python.ipynb g > TOC.md`

Include it in the jupyter notebook

```python

%load_ext autoreload

%autoreload

from jupyter-toc-gen import program as toc 

toc.main("relative/path/to/python.ipynb", "g")

```

Then copy the output to a markdown cell and voila. 

## Disclaimer

The script will ignore cells marked with "Table of Contents" in the first line of a cell. Meaning that the scraper will ignore the TOC cell itself.