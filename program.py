import os
import sys
import json

def main(m_path="", m_action=""):
    
    fn = ""
    action = ""
    
    if m_path != "" and m_action != "":
        fn = m_path
        action = m_action
    elif len(sys.argv) == 1:
        print("Choose jupyter notebook file with markdown headings:")
        fn = input("> ")
        print("Choose an option for generating TOC:")
        print("  g - generate")
        print("  c - cancel")
        action = input("> ")
    elif len(sys.argv) == 2 and sys.argv[1] == "-h":
        print("""program [PATH] [ACTION]

  PATH = file path
  ACTION = g, generate or c, cancel
  """)
        return
    elif len(sys.argv) == 3:
        fn = sys.argv[1]
        action = sys.argv[2]
        
    fo = open(str(fn), "r")
    assert os.path.exists(fn), "Notebook not found at: " + str(fn)
    
    if action == "g":
        file = json.load(fo)
        assert len(file) != 0, "The file couldn't be loaded"
        
        md_cells = []
        
        # scrape json for markdown cells with hashtags
        for cell in file["cells"]:
            if cell["cell_type"] == "markdown":
                toc_found = False
                for hstr in cell["source"]:
                    if "Table of Contents" in hstr:
                        toc_found = True
                    if "#" in hstr and toc_found == False:
                        md_cells.append(hstr)
        # format output
        print("=== Output begin ===")
        print("# Table of Contents\n")

        for cell in md_cells:
            cell = cell.replace('\n', '')
            cell = cell.replace('#', '', 1)
            text = cell.replace('#','   ')
            spaces = text[text.index(" "):len(text) - len(text.lstrip(" "))]
            text = text.lstrip(" ")
            link = cell.replace("#", '')
            link = link.lstrip(" ")
            link = link.replace(" ", "-")
            print(f"{spaces}- [{text}](#{link})") 
        print("=== END OUTPUT ===")
    else:
        print("Exiting...")
        return

if __name__ == "__main__":
    main()
